package com.example.msextconnection.repository;

import com.example.msextconnection.entity.FileData;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FileDataRepository extends JpaRepository<FileData, Long> {

    Page<FileData> findByFileNameContaining(String fileName, Pageable pageable);

    List<FileData> findByFileNameContaining(String fileName, Sort sort);
}
