package com.example.msdatahandler.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class CrossConfig implements WebMvcConfigurer {

    private static  final String[] METHOD= {"GET","POST","OPTIONS","PUT","PATCH","DELETE"};

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**") // What request is intercepted
                .allowedOriginPatterns("*")
                .allowedHeaders("*")
                .allowedMethods(METHOD)
                .allowCredentials(false)
                .maxAge(6000);
    }
}
