package com.example.ext_app.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import javax.xml.transform.TransformerException;

@Slf4j
@RequiredArgsConstructor
@Service
public class ExtConnectionReceiver {

    private final XMLFileHandler xmlFileHandler;

    @KafkaListener(topics = "${kafka.topics.topic3}")
    public void receive(ConsumerRecord<String, String> consumerRecord) throws TransformerException {

        log.info("kafka key: {}, value: {}", consumerRecord.key(), consumerRecord.value());

        xmlFileHandler.transformToXmlFile(consumerRecord.key(), consumerRecord.value());
    }
}
