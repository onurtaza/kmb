package com.example.msdatahandler.mapper;

import com.example.msdatahandler.dto.ActPeriodWorking;
import com.example.msdatahandler.entity.ActPeriodWorkingEntity;
import com.example.msdatahandler.entity.PeriodEntity;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import static com.example.msdatahandler.TestObjectsFactory.createActPeriodWorking;
import static com.example.msdatahandler.TestObjectsFactory.createPeriod;
import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
@SpringBootTest
public class MappingTest {

    @Autowired
    private CommonMapper mapper;

    @Test
    void periods_whenMaps_thenCorrect() {

        PeriodEntity periodEntity = mapper.toEntity(createPeriod(2021, 1, 1));

        assertThat(periodEntity.getReportDate()).isEqualTo(LocalDate.of(2021, 1, 1));
        assertThat(periodEntity.getReportYear()).isEqualTo(2021);
        assertThat(periodEntity.getReportMonth()).isEqualTo(1);
        assertThat(periodEntity.getJobName()).isEqualTo("Jedi");

        log.info(periodEntity.toString());
    }

    @Test
    void actPeriodWorking_whenMaps_thenCorrect() {

        ActPeriodWorking actPeriodWorking = createActPeriodWorking();

        ActPeriodWorkingEntity actPeriodWorkingEntity = mapper.toEntity(actPeriodWorking);

        assertThat(actPeriodWorkingEntity.getName()).isEqualTo("Luke");

        PeriodEntity periodEntity1 = mapper.toEntity(createPeriod(2015, 11, 3));
        PeriodEntity periodEntity2 = mapper.toEntity(createPeriod(2021, 1, 1));

        Set<PeriodEntity> periodEntitySet = new HashSet<>();
        periodEntitySet.add(periodEntity1);
        periodEntitySet.add(periodEntity2);

        assertThat(actPeriodWorkingEntity.getPeriods()).isEqualTo(periodEntitySet);

        log.info(actPeriodWorkingEntity.toString());
    }
}
