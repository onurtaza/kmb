package com.example.msdatahandler.controller;

import com.example.msdatahandler.client.FeignFileDataClient;
import com.example.msdatahandler.entity.ActPeriodWorkingEntity;
import com.example.msdatahandler.entity.PassportDataEntity;
import com.example.msdatahandler.mapper.CommonMapper;
import com.example.msdatahandler.repository.ActPeriodWorkingRepository;
import com.example.msdatahandler.repository.PassportDataRepository;
import com.google.inject.internal.cglib.core.$ClassEmitter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/data")
public class ExtConnectionController {

    @Value("${xjc.path}")
    private String xjcPath;

    private final PassportDataRepository passportDataRepository;
    private final ActPeriodWorkingRepository actPeriodWorkingRepository;
    private final CommonMapper mapper;
    private final FeignFileDataClient client;

    /**
     * Data come from frontend.
     * Definition xml data by ID and send that to "data-handler" microservice
     *
     * @param entityId ID of xml-data in database
     * @param fileName Actual content of xml-data
     * @throws JAXBException print exception if occurs conversion error
     */

    @PostMapping("/send")
    public void sendToCheck(
            @RequestParam(value = "id") Long entityId,
            @RequestParam(value = "fileName") String fileName) throws JAXBException {

        JAXBContext jc = JAXBContext.newInstance(xjcPath);
        Marshaller u = jc.createMarshaller();
        StringWriter writer = new StringWriter();

        Object dto = null;

        if (fileName.toLowerCase().contains("passportdata")) {
            PassportDataEntity entity = passportDataRepository.findById(entityId)
                    .orElseThrow(() -> new RuntimeException("PassportData not found"));
            dto = mapper.toDTO(entity);
        } else if (fileName.toLowerCase().contains("actperiodworking")) {
            ActPeriodWorkingEntity entity = actPeriodWorkingRepository.findById(entityId)
                    .orElseThrow(() -> new RuntimeException("ActPeriodWorking not found"));
            dto = mapper.toDTO(entity);
        }

        if (dto == null) {
            throw new RuntimeException("Entity not found");
        }

        u.marshal(dto, writer);

        log.info(writer.toString());

        client.sendToCheck(entityId, writer.toString());
    }
}
