package com.example.msextconnection.mapper;

import com.example.msextconnection.dto.FileDataDTO;
import com.example.msextconnection.entity.FileData;
import org.mapstruct.Mapper;
import org.springframework.data.domain.Page;

import java.util.List;

@Mapper(componentModel = "spring")
public interface FileDataMapper {

    FileData toEntity(FileDataDTO fileDataDTO);

    FileDataDTO toDTO(FileData fileData);

    List<FileDataDTO> toDTO(Page<FileData> fileDataPage);
}
