package com.example.msdatahandler.service;

import com.example.msdatahandler.dto.ActPeriodWorking;
import com.example.msdatahandler.dto.PassportData;
import com.example.msdatahandler.entity.ActPeriodWorkingEntity;
import com.example.msdatahandler.entity.PassportDataEntity;
import com.example.msdatahandler.mapper.CommonMapper;
import com.example.msdatahandler.repository.ActPeriodWorkingRepository;
import com.example.msdatahandler.repository.PassportDataRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;

@Service
@RequiredArgsConstructor
public class XMLDataHandler {

    @Value("${xjc.path}")
    private String xjcPath;

    private final CommonMapper mapper;
    private final ActPeriodWorkingRepository actPeriodWorkingRepository;
    private final PassportDataRepository passportDataRepository;

    /**
     * This method used for conversion xml in string format to DTO object
     * and mapping their to entity object and save
     *
     * @param fileNameAndId Consists of file name and file ID
     * @param xmlString Main content
     * @throws JAXBException print exception in case of conversion
     */

    public void handle(String fileNameAndId, String xmlString) throws JAXBException {

        JAXBContext jc = JAXBContext.newInstance(xjcPath);
        Unmarshaller u = jc.createUnmarshaller();
        StringReader reader = new StringReader(xmlString);

        Long id = Long.parseLong(fileNameAndId.split("_")[1]);

        if (fileNameAndId.toLowerCase().contains("passportdata")) {
            PassportData passportDataDTO = (PassportData) u.unmarshal(reader);
            PassportDataEntity entity = mapper.toEntity(passportDataDTO);
            entity.setId(id);
            passportDataRepository.save(entity);
        } else if (fileNameAndId.toLowerCase().contains("actperiodworking")) {
            ActPeriodWorking actPeriodWorkingDTO = (ActPeriodWorking) u.unmarshal(reader);
            ActPeriodWorkingEntity entity = mapper.toEntity(actPeriodWorkingDTO);
            entity.setId(id);
            actPeriodWorkingRepository.save(entity);
        }
    }
}
