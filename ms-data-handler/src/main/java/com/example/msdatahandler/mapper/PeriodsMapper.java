package com.example.msdatahandler.mapper;

import com.example.msdatahandler.dto.ActPeriodWorking;
import com.example.msdatahandler.dto.Period;
import com.example.msdatahandler.entity.PeriodEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
@RequiredArgsConstructor
public class PeriodsMapper {

    private final CommonMapper mapper;

    public Set<PeriodEntity> map(ActPeriodWorking.Periods periods) {
        Set<PeriodEntity> periodEntitySet = new HashSet<>();

        periods.getPeriod()
                .forEach(p -> periodEntitySet.add(mapper.toEntity(p)));

        return periodEntitySet;
    }

    public ActPeriodWorking.Periods map(Set<PeriodEntity> periodEntitySet) {
        List<Period> period = new ArrayList<>();

        periodEntitySet.forEach(p -> period.add(mapper.toDTO(p)));

        ActPeriodWorking.Periods periods = new ActPeriodWorking.Periods();

//        periods.setPeriod(period);

        return periods;
    }
}
