package com.example.ext_app.controller;

import com.example.ext_app.service.XMLFileHandler;
import com.example.ext_app.service.Producer;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@RequestMapping("kafka")
public class KafkaController {

    private final Producer producer;
    private final XMLFileHandler xmlFileHandler;

    @PostMapping("/send")
    public String sendData(@RequestParam("fileName") String fileName) {
        String xmlString = xmlFileHandler.getXMLStringByFileName(fileName);
        producer.publishToTopic(fileName, xmlString);

        return "Published data: key = " + fileName + ", value = " + xmlString;
    }
}
