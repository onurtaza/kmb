package com.example.msextconnection.entity;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.ZonedDateTime;

@Data
@Entity
@Table(name = "file_data", schema = "public")
public class FileData {

    @Id
    @GeneratedValue
    @Setter(AccessLevel.PRIVATE)
    private Long id;

    @Column(name = "created_date")
    @Setter(AccessLevel.PRIVATE)
    private ZonedDateTime createdDate;

    @PrePersist
    private void onSave() {
        this.createdDate = ZonedDateTime.now();
    }

    @Column(name = "file_name")
    private String fileName;

    @Type(type = "com.example.msextconnection.type.SQLXMLType")
    @Column(columnDefinition = "xml")
    private String content;
}
