import {Component, OnInit} from '@angular/core';
import {BehaviorSubject, of} from "rxjs";
import {FormBuilder} from "@angular/forms";
import {ActivatedRoute} from "@angular/router";
import {catchError, finalize} from "rxjs/operators";
import * as _ from 'lodash';
import PassportData from "../../models/passport-data";
import {ActPeriodWorkingService} from "../../services/act-period-working.service";
import ActPeriodWorking from "../../models/act-period-working.model";

@Component({
  selector: 'app-act-period-working',
  templateUrl: './act-period-working.component.html',
  styleUrls: ['./act-period-working.component.css']
})
export class ActPeriodWorkingComponent implements OnInit {

  private loadingSubject = new BehaviorSubject<boolean>(false);
  private actPeriodWorkingId = this.route.snapshot.paramMap.get("id");

  constructor(
    private formBuilder: FormBuilder,
    private actPeriodWorkingService: ActPeriodWorkingService,
    private route: ActivatedRoute) {
  }

  form = this.formBuilder.group({
    surname: [''],
    name: [''],
    patronymic: [''],
    periodStart: [''],
    periodEnd: [''],
    periods: ['']
  });

  ngOnInit(): void {
    this.populateForm(this.actPeriodWorkingId);
  }

  saveForm(): void {
    let params = this.form.value;
    params['id'] = this.actPeriodWorkingId;
    console.log('params', params);
    this.actPeriodWorkingService.saveData(params)
  }

  backToList(): void {
    window.open('/', '_self');
  }

  populateForm(id): void {
    this.loadingSubject.next(true);
    this.actPeriodWorkingService.getDataById(id)
      .pipe(
        catchError(() => of(null)),
        finalize(() => this.loadingSubject.next(false))
      )
      .subscribe((result: ActPeriodWorking) => {
          console.info("result", result);
          this.form.setValue(_.omit(result, 'id'));
        }
      );
  }
}
