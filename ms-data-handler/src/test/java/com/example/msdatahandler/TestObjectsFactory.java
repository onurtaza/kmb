package com.example.msdatahandler;

import com.example.msdatahandler.dto.ActPeriodWorking;
import com.example.msdatahandler.dto.Period;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class TestObjectsFactory {

    private static DatatypeFactory datatypeFactory;

    static {
        try {
            datatypeFactory = DatatypeFactory.newInstance();
        } catch (DatatypeConfigurationException e) {
            e.printStackTrace();
        }
    }

    public static Period createPeriod(int year, int month, int day) {

        Period period = new Period();

        period.setReportDate(datatypeFactory.newXMLGregorianCalendarDate(year, month, day, DatatypeConstants.FIELD_UNDEFINED));
        period.setReportYear(BigInteger.valueOf(year));
        period.setReportMonth(BigInteger.valueOf(month));
        period.setJobName("Jedi");

        return period;
    }

    public static ActPeriodWorking createActPeriodWorking() {

        ActPeriodWorking actPeriodWorking = new ActPeriodWorking();
        ActPeriodWorking.Periods periods = new ActPeriodWorking.Periods();

        List<Period> periodList  = new ArrayList<>();
        periodList.add(createPeriod(2015, 11, 3));
        periodList.add(createPeriod(2021, 1, 1));
        periods.setPeriod(periodList);

        actPeriodWorking.setName("Luke");
        actPeriodWorking.setSurname("Skywalker");
        actPeriodWorking.setPatronymic("Anakinson");
        actPeriodWorking.setPeriodStart(datatypeFactory.newXMLGregorianCalendarDate(2010, 1, 1, DatatypeConstants.FIELD_UNDEFINED));
        actPeriodWorking.setPeriodEnd(datatypeFactory.newXMLGregorianCalendarDate(2021, 1, 1, DatatypeConstants.FIELD_UNDEFINED));
        actPeriodWorking.setPeriods(periods);

        return actPeriodWorking;
    }
}
