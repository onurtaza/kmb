package com.example.msdatahandler.repository;

import com.example.msdatahandler.entity.ActPeriodWorkingEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ActPeriodWorkingRepository extends JpaRepository<ActPeriodWorkingEntity, Long> {

    Optional<ActPeriodWorkingEntity> findByName(String name);
    Optional<List<ActPeriodWorkingEntity>> findAllByName(String name);
}
