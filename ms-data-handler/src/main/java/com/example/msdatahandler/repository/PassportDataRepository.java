package com.example.msdatahandler.repository;

import com.example.msdatahandler.entity.PassportDataEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PassportDataRepository extends JpaRepository<PassportDataEntity, Long> {
}
