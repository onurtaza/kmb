package com.example.msdatahandler.mapper;

import com.example.msdatahandler.dto.ActPeriodWorking;
import com.example.msdatahandler.dto.PassportData;
import com.example.msdatahandler.dto.Period;
import com.example.msdatahandler.entity.ActPeriodWorkingEntity;
import com.example.msdatahandler.entity.PassportDataEntity;
import com.example.msdatahandler.entity.PeriodEntity;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

@Mapper(uses = PeriodsMapper.class, componentModel = "spring")
public abstract class CommonMapper {

    @AfterMapping
    protected void configPeriods(@MappingTarget ActPeriodWorkingEntity actPeriodWorkingEntity) {

        for (PeriodEntity periodEntity : actPeriodWorkingEntity.getPeriods()) {
            periodEntity.setActPeriodWorkingEntity(actPeriodWorkingEntity);
        }
    }

    public abstract ActPeriodWorkingEntity toEntity(ActPeriodWorking actPeriodWorking);

    public abstract ActPeriodWorking toDTO(ActPeriodWorkingEntity actPeriodWorkingEntity);

    public abstract PeriodEntity toEntity(Period period);

    public abstract Period toDTO(PeriodEntity periodEntity);

    public abstract PassportDataEntity toEntity(PassportData passportData);

    public abstract PassportData toDTO(PassportDataEntity passportDataEntity);
}
