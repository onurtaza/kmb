package com.example.msdatahandler.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "ms-ext-connection")
public interface FeignFileDataClient {
    @PostMapping("/ext-con/send")
    void sendToCheck(
            @RequestParam(value = "id") Long id,
            @RequestParam(value = "xmlString") String xmlString);
}
