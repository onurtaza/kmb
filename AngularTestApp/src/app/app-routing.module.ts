import {RouterModule, Routes} from "@angular/router";
import {FileDataListComponent} from "./components/file-data-list/file-data-list.component";
import {NgModule} from "@angular/core";
import {PassportDataComponent} from "./components/passport-data/passport-data.component";
import {ActPeriodWorkingComponent} from "./components/act-period-working/act-period-working.component";

const routes: Routes = [
  { path: '', redirectTo: 'file-data', pathMatch: 'full' },
  { path: 'file-data', component: FileDataListComponent },
  { path: 'passportdata/:id', component: PassportDataComponent },
  { path: 'actperiodworking/:id', component: ActPeriodWorkingComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
