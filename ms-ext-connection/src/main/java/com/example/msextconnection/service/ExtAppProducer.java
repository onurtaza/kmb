package com.example.msextconnection.service;

import com.example.msextconnection.config.KafkaProperties;
import com.example.msextconnection.entity.FileData;
import lombok.AllArgsConstructor;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class ExtAppProducer {
    private final KafkaProperties kafkaProperties;
    private final KafkaTemplate<String, String> kafkaTemplate;

    /**
     * Transfer the xml-data to "external application"
     * @param fileData xml-object, which consists from file name and content
     */

    public void publishToTopic(FileData fileData) {
        kafkaTemplate.send(kafkaProperties.getTopics().getTopic3(),
                fileData.getFileName(), fileData.getContent());
    }
}
