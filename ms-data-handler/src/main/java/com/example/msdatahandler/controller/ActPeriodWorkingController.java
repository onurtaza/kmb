package com.example.msdatahandler.controller;

import com.example.msdatahandler.entity.ActPeriodWorkingEntity;
import com.example.msdatahandler.repository.ActPeriodWorkingRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

@RestController
@Slf4j
@AllArgsConstructor
@RequestMapping("/data/act-period-working")
public class ActPeriodWorkingController {

    private final ActPeriodWorkingRepository repository;
    private final ObjectMapper objectMapper;

    @GetMapping("/{id}")
    public ActPeriodWorkingEntity getDataById(@PathVariable String id) {
        ActPeriodWorkingEntity entity =  repository.findById(Long.parseLong(id))
                .orElseThrow(() -> new RuntimeException("ActPeriodWorking not found!"));

        return entity;
    }

    @PostMapping
    public void saveData(@RequestBody @NotNull String data) throws JsonProcessingException {
        ActPeriodWorkingEntity entity = objectMapper.readValue(data, ActPeriodWorkingEntity.class);
        log.info(entity.toString());

        repository.save(entity);
    }
}
