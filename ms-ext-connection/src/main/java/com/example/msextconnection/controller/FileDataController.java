package com.example.msextconnection.controller;

import com.example.msextconnection.dto.FileDataDTO;
import com.example.msextconnection.entity.FileData;
import com.example.msextconnection.mapper.FileDataMapper;
import com.example.msextconnection.repository.FileDataRepository;
import com.example.msextconnection.service.ExtAppProducer;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.*;
import org.springframework.data.domain.Sort.Order;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 *
 */

@RestController
@Slf4j
@RequestMapping("/ext-con")
@AllArgsConstructor
@CrossOrigin(origins = "http://localhost:4200")
public class FileDataController {

    private final FileDataRepository fileDataRepository;
    private final FileDataMapper mapper;
    private final ExtAppProducer extAppProducer;

    /**
     * this method used for passing list of xml-data to frontend and showing on UI
     *
     * @param fileName Needs to filter list by fileName
     * @param page Needs to pagination on UI, set the current page
     * @param size Needs to pagination on UI, set the size of elements on page
     * @param sort Sorts the column name "File name"on UI
     * @return returns the filtered, sorted, paginated list of xml-data
     */

    @GetMapping("/files")
    public Page<FileDataDTO> showSavedFiles(
            @RequestParam(required = false) String fileName,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "3") int size,
            @RequestParam(defaultValue = "asc") String sort
    ) {
        Pageable pagingSort = PageRequest.of(page, size, Sort.by(new Order(getSortDirection(sort), "fileName")));

        Page<FileData> fileDataPage;
        if (fileName == null) {
            fileDataPage = fileDataRepository.findAll(pagingSort);
        } else {
            fileDataPage = fileDataRepository.findByFileNameContaining(fileName, pagingSort);
        }

        List<FileDataDTO> fileDataDTOList = mapper.toDTO(fileDataPage);

        return new PageImpl<>(fileDataDTOList, pagingSort, fileDataPage.getTotalElements());
    }

    /**
     * this method used for save changed data from UI system to database
     * and subsequent transfer their to "external system"
     *
     * @param id Needs to update data by ID in database
     * @param xmlString Content for updating By ID
     */

    @PostMapping("/send")
    public void sendToCheck(
            @RequestParam(value = "id") Long id,
            @RequestParam(value = "xmlString") String xmlString
    ) {
        FileData fileData = fileDataRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("FileData not found"));
        fileData.setContent(xmlString);

        fileDataRepository.save(fileData);

        log.info(fileData.toString());

        extAppProducer.publishToTopic(fileData);
    }

    private Sort.Direction getSortDirection(String sort) {
        return sort.toLowerCase().contains("asc") ? Sort.Direction.ASC : Sort.Direction.DESC;
    }
}
