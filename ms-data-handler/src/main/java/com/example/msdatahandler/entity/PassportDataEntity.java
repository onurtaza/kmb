package com.example.msdatahandler.entity;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
@Table(name = "passport_data")
public class PassportDataEntity {

    @Id
    private Long id;

    @Column
    private String surname;

    @Column
    private String name;

    @Column
    private String patronymic;

    @Column(name = "birth_date")
    private LocalDate birthDate;

    @Column(name = "issue_date")
    private LocalDate issueDate;

    @Column
    private String serial;

    @Column
    private String number;

    @Column(name = "authority_name")
    private String authorityName;
}
