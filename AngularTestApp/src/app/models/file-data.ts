export interface FileDataResponse {
  content: FileData[];
  totalElements: number;
  isClicked: boolean;
}

export interface FileData {
  id: number;
  fileName: string;
  createdDate: string
}
