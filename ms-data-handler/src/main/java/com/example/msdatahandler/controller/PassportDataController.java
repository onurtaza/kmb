package com.example.msdatahandler.controller;

import com.example.msdatahandler.entity.PassportDataEntity;
import com.example.msdatahandler.repository.PassportDataRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

@RestController
@Slf4j
@AllArgsConstructor
@RequestMapping("/data/passport-data")
public class PassportDataController {

    private final PassportDataRepository repository;
    private final ObjectMapper objectMapper;

    @GetMapping("/{id}")
    public PassportDataEntity getDataById(@PathVariable String id) {
        return repository.findById(Long.parseLong(id))
                .orElseThrow(() -> new RuntimeException("PassportData not found!"));
    }

    @PostMapping
    public void saveData(@RequestBody @NotNull String data) throws JsonProcessingException {
        PassportDataEntity entity = objectMapper.readValue(data, PassportDataEntity.class);
        log.info(entity.toString());

        repository.save(entity);
    }
}
