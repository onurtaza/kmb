import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActPeriodWorkingComponent } from './act-period-working.component';

describe('ActPeriodWorkingComponent', () => {
  let component: ActPeriodWorkingComponent;
  let fixture: ComponentFixture<ActPeriodWorkingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActPeriodWorkingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActPeriodWorkingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
