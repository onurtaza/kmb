export default class Period {
  reportYear: number;
  reportMonth: number;
  reportDate: Date;
  jobName: string;
}
