package com.example.msextconnection.service;

import com.example.msextconnection.dto.FileDataDTO;
import com.example.msextconnection.entity.FileData;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
public class ExtAppReceivedMessageHandler {

    private final FileDataHandler fileDataHandler;
    private final DataHandlerProducer producer;

    /**
     * This method saves the xml-data in database
     * and send their to "ms-data handler" microservice
     * @param fileDataDTO xml-data meant to updating and transfer
     */

    public void handle(FileDataDTO fileDataDTO) {
        FileData fileData = fileDataHandler.update(fileDataDTO);
        producer.publishToTopic(fileData);
    }
}
