import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FileDataService} from "../../services/file-data.service";
import {FileDataDatasource} from "../../datasources/file-data.datasource";
import {MatPaginator} from "@angular/material/paginator";
import {debounceTime, distinctUntilChanged, tap} from "rxjs/operators";
import {MatSort} from "@angular/material/sort";
import {fromEvent} from "rxjs";
import {Router} from "@angular/router";

@Component({
  selector: 'app-file-data-list',
  templateUrl: './file-data-list.component.html',
  styleUrls: ['./file-data-list.component.css']
})
export class FileDataListComponent implements AfterViewInit, OnInit {

  displayedColumns = ['id', 'fileName', 'createdDate', 'actions'];
  fileDataDatasource: FileDataDatasource;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('input') input: ElementRef;

  constructor(private fileDataService: FileDataService,
              private router: Router) {
  }

  ngOnInit() {
    this.fileDataDatasource = new FileDataDatasource(this.fileDataService);
    this.fileDataDatasource.getAll();
  }

  ngAfterViewInit() {
    this.fileDataDatasource.counter$
      .pipe(
        tap((count) => {
          this.paginator.length = count;
        })
      )
      .subscribe();

    // server-side search
    fromEvent(this.input.nativeElement, 'keyup')
      .pipe(
        debounceTime(500),
        distinctUntilChanged(),
        tap(() => {
          this.paginator.pageIndex = 0;
          this.getAll();
        })
      )
      .subscribe();

    // reset the paginator after sorting
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    // on sort or paginate events, load a new page
    this.paginator.page
      .pipe(
        tap(() => this.getAll())
      )
      .subscribe();

    this.sort.sortChange
      .pipe(
        tap(() => this.getAll())
      )
      .subscribe();
  }

  getAll() {
    this.fileDataDatasource.getAll(
      this.input.nativeElement.value,
      this.paginator.pageIndex,
      this.paginator.pageSize,
      this.sort.direction);
  }

  onRowClicked(row) {
    const fileName = row.fileName.toLowerCase().split('.')[0];
    this.router.navigateByUrl(`${fileName}/${row.id}`);
  }

  sendToCheck(row, e) {
    console.log('Send to check row: ', row);

    this.fileDataService.sendToCheck(row.id, row.fileName);

    row.isClicked = true;
    e.stopPropagation();
  }
}
