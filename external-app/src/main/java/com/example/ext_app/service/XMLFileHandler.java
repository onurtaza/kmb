package com.example.ext_app.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.nio.file.Files;

@Service
@Slf4j
public class XMLFileHandler {

    @Value("${filePath}")
    String filePath;

    public String getXMLStringByFileName(String fileName) {
        String stringData = null;
        try {
            File file = ResourceUtils.getFile(filePath + fileName);
            stringData = new String(Files.readAllBytes(file.toPath()));
        } catch (IOException e) {
            log.error("No such file");
            e.printStackTrace();
        }

        return stringData;
    }

    public void transformToXmlFile(String fileName, String xmlString) throws TransformerException {
        Transformer transformer = createXmlTransformer();

        Document document = convertStringToXMLDocument(xmlString);

        Result result;
        try {
            File xmlFile = ResourceUtils.getFile(filePath + fileName);
            result = new StreamResult(new PrintWriter(
                    new FileOutputStream(xmlFile, false)));
        } catch (FileNotFoundException e) {
            result = new StreamResult(new File(fileName));
        }

        Source input = new DOMSource(document);

        transformer.transform(input, result);
    }

    private static Document convertStringToXMLDocument(String xmlString) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        DocumentBuilder builder = null;
        try {
            builder = factory.newDocumentBuilder();

            Document doc =  builder.parse(new InputSource(new StringReader(xmlString)));
            return doc;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static Transformer createXmlTransformer() throws TransformerConfigurationException {
        Transformer transformer = TransformerFactory.newInstance()
                .newTransformer();
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        return transformer;
    }
}
