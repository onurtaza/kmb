package com.example.msextconnection.service;

import com.example.msextconnection.dto.FileDataDTO;
import com.example.msextconnection.entity.FileData;
import com.example.msextconnection.repository.FileDataRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class FileDataHandler {

    private final FileDataRepository fileDataRepository;

    public FileData update(FileDataDTO fileDataDTO) {
        FileData fileData = new FileData();

        fileData.setFileName(fileDataDTO.getFileName());
        fileData.setContent(fileDataDTO.getContent());
        return fileDataRepository.save(fileData);
    }
}
