package com.example.msdatahandler.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBException;

@Slf4j
@Service
@RequiredArgsConstructor
public class ExtConnectionReceiver {

    private final XMLDataHandler xmlDataHandler;

    @KafkaListener(topics = "${kafka.topics.topic2}")
    public void receive(ConsumerRecord<String, String> consumerRecord) throws JAXBException {

        log.info("kafka key: {}, value: {}", consumerRecord.key(), consumerRecord.value());

        xmlDataHandler.handle(consumerRecord.key(), consumerRecord.value());
    }

}
