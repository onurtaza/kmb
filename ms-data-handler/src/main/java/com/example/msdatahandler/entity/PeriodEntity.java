package com.example.msdatahandler.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
@Table(name = "period")
@EqualsAndHashCode(callSuper = true, exclude = "actPeriodWorkingEntity")
public class PeriodEntity extends BaseEntity {

    @Column
    private Integer reportYear;

    @Column
    private Integer reportMonth;

    @Column
    private LocalDate reportDate;

    @Column
    private String jobName;

    @ToString.Exclude
    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "act_period_working_id", nullable = false)
    private ActPeriodWorkingEntity actPeriodWorkingEntity;
}
