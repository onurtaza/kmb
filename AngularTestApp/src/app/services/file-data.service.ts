import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class FileDataService {

  constructor(private http: HttpClient) {
  }

  getAll(params: any) {
    const endpoint = environment.apiUrl + environment.extConnectionUrl + "/files";
    return this.http.get(endpoint, {params});
  }

  sendToCheck(id, fileName) {
    const endpoint = environment.apiUrl + environment.dataHandlerUrl + "/send";
    return this.http.post(endpoint, null, {params: {id: id, fileName: fileName}})
      .subscribe(data => console.log(data));
  }


}
