package com.example.msextconnection;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.kafka.annotation.EnableKafka;

@EnableEurekaClient
@EnableKafka
@SpringBootApplication
public class ServiceToConnectionExtAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiceToConnectionExtAppApplication.class, args);
	}

}
