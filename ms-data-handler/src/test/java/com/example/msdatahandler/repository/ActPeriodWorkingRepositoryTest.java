package com.example.msdatahandler.repository;

import com.example.msdatahandler.TestObjectsFactory;
import com.example.msdatahandler.dto.ActPeriodWorking;
import com.example.msdatahandler.entity.ActPeriodWorkingEntity;
import com.example.msdatahandler.entity.PeriodEntity;
import com.example.msdatahandler.mapper.CommonMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Slf4j
@SpringBootTest
class ActPeriodWorkingRepositoryTest {

    @Autowired
    private ActPeriodWorkingRepository actPeriodWorkingRepository;

    @Autowired
    private CommonMapper mapper;

    private final String name = "Luke";
    private final Long id = Long.MAX_VALUE;

    @AfterEach
    void cleanup() {
        actPeriodWorkingRepository
                .findAllByName(name)
                .ifPresent(list -> actPeriodWorkingRepository.deleteAll(list));
    }

    @Test
    void save_actPeriodWorkingEntity() {

        ActPeriodWorkingEntity actPeriodWorkingEntity = new ActPeriodWorkingEntity();
        actPeriodWorkingEntity.setId(id);
        actPeriodWorkingEntity.setName(name);
        actPeriodWorkingEntity.setSurname("Skywalker");
        actPeriodWorkingEntity.setPatronymic("Anakinson");

        PeriodEntity periodEntity = new PeriodEntity();
        periodEntity.setReportDate(LocalDate.of(2021, 1, 1));
        periodEntity.setReportYear(2021);
        periodEntity.setReportMonth(1);
        periodEntity.setJobName("Jedi Master");

        Set<PeriodEntity> periods = new HashSet<>();
        periods.add(periodEntity);

        actPeriodWorkingEntity.setPeriods(periods);
        periodEntity.setActPeriodWorkingEntity(actPeriodWorkingEntity);

        actPeriodWorkingRepository.save(actPeriodWorkingEntity);

        log.info(actPeriodWorkingEntity.toString());
    }

    @Test
    void save_actPeriodWorkingViaDTOToEntityMapper() {

        ActPeriodWorking actPeriodWorking = TestObjectsFactory.createActPeriodWorking();

        ActPeriodWorkingEntity actPeriodWorkingEntity = mapper.toEntity(actPeriodWorking);
        actPeriodWorkingEntity.setId(id);

        actPeriodWorkingRepository.save(actPeriodWorkingEntity);

        log.info(actPeriodWorkingEntity.toString());
    }
}
