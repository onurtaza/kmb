package com.example.ext_app.service;

import com.example.ext_app.config.KafkaProperties;
import lombok.AllArgsConstructor;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class Producer {

    private final KafkaProperties kafkaProperties;
    private final KafkaTemplate<String, String> kafkaTemplate;

    public void publishToTopic(String key, String value) {
        kafkaTemplate.send(kafkaProperties.getTopics().getTopic1(), key, value);
    }

}
