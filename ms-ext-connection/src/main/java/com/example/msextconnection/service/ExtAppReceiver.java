package com.example.msextconnection.service;

import com.example.msextconnection.dto.FileDataDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service
public class ExtAppReceiver {

    private final ExtAppReceivedMessageHandler receivedMessageHandler;

    /**
     * Receiving xml-data from "external application"
     * @param consumerRecord Consists of filename and xml-data in string format
     */

    @KafkaListener(topics = "${kafka.topics.topic1}")
    public void receive(ConsumerRecord<String, String> consumerRecord) {

        log.info("kafka key: {}, value: {}", consumerRecord.key(), consumerRecord.value());

        receivedMessageHandler.handle(new FileDataDTO(consumerRecord.key(), consumerRecord.value()));
    }
}
