package com.example.msextconnection.config;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

javadoc

@Data
@Component
@ConfigurationProperties("kafka")
public class KafkaProperties {

    private Topics topics;

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Topics {
        private String topic1;
        private String topic2;
        private String topic3;
    }
}
