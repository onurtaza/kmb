export default interface PassportData {
  id: string;
  name: string;
  surname: string;
  patronymic: string;
  birthDate: Date;
  issueDate: Date;
  serial: string;
  number: number;
  authorityName: string;
}
