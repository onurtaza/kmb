package com.example.msextconnection.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;

@Data
@NoArgsConstructor
public class FileDataDTO {

    private Long id;

    private ZonedDateTime createdDate;

    private String fileName;

    private String content;

    public FileDataDTO(
            @NotNull(message = "validation.field.null")
            @NotEmpty(message = "validation.field.empty") String fileName,
            @NotNull(message = "validation.field.null")
            @NotEmpty(message = "validation.field.empty") String content) {
        this.fileName = fileName;
        this.content = content;
    }
}
