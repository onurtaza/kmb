import Period from "./period.model";

export default interface ActPeriodWorking {
  id: string;
  name: string;
  surname: string;
  patronymic: string;
  periodStart: Date;
  periodEnd: Date;
  periods: Array<Partial<Period>>
}
