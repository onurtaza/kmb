import {Component, Injectable, OnInit} from '@angular/core';
import {FormBuilder} from "@angular/forms";
import {PassportDataService} from "../../services/passport-data.service";
import {catchError, finalize} from "rxjs/operators";
import {BehaviorSubject, of} from "rxjs";
import PassportData from "../../models/passport-data";
import * as _ from 'lodash';
import {ActivatedRoute} from "@angular/router";


@Component({
  selector: 'app-passport-data',
  templateUrl: './passport-data.component.html',
  styleUrls: ['./passport-data.component.css']
})

@Injectable({
  providedIn: 'root'
})

export class PassportDataComponent implements OnInit {

  private loadingSubject = new BehaviorSubject<boolean>(false);
  private passportDataId = this.route.snapshot.paramMap.get("id");

  constructor(
    private formBuilder: FormBuilder,
    private passportDataService: PassportDataService,
    private route: ActivatedRoute) {
  }

  form = this.formBuilder.group({
    surname: [''],
    name: [''],
    patronymic: [''],
    birthDate: [''],
    issueDate: [''],
    serial: [''],
    number: [''],
    authorityName: ['']
  });

  ngOnInit(): void {
    this.populateForm(this.passportDataId);
  }

  saveForm(): void {
    let params = this.form.value;
    params['id'] = this.passportDataId;
    console.log('params', params);
    this.passportDataService.saveData(params)
  }

  backToList(): void {
    window.open('/', '_self');
  }

  populateForm(id): void {
    this.loadingSubject.next(true);
    this.passportDataService.getDataById(id)
      .pipe(
        catchError(() => of(null)),
        finalize(() => this.loadingSubject.next(false))
      )
      .subscribe((result: PassportData) => {
          console.info("result", result);
          this.form.setValue(_.omit(result, 'id'));
        }
      );
  }

}
