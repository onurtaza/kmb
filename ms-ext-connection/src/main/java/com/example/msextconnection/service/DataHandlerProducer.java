package com.example.msextconnection.service;

import com.example.msextconnection.config.KafkaProperties;
import com.example.msextconnection.entity.FileData;
import lombok.AllArgsConstructor;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class DataHandlerProducer {
    private final KafkaProperties kafkaProperties;
    private final KafkaTemplate<String, String> kafkaTemplate;

    /**
     * Send the xml-data to "data-handler" microservice for further processing
     */

    public void publishToTopic(FileData fileData) {
        kafkaTemplate.send(kafkaProperties.getTopics().getTopic2(),
                fileData.getFileName() + "_" + fileData.getId(),
                fileData.getContent());
    }
}
