import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "../../environments/environment";
import PassportData from "../models/passport-data";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'text/plain',
  })
};

@Injectable({
  providedIn: 'root'
})
export class PassportDataService {

  private url = environment.apiUrl + environment.dataHandlerUrl + "/passport-data";

  constructor(private http: HttpClient) {
  }

  getDataById(id: string) {
    const endpoint = this.url + "/" + id;

    return this.http.get<PassportData>(endpoint);
  }

  saveData(body: PassportData) {
    this.http.post(this.url, body, httpOptions).subscribe(data => console.info('data', data));
  }
}
