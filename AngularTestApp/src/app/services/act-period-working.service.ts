import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "../../environments/environment";
import ActPeriodWorking from "../models/act-period-working.model";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'text/plain',
  })
};

@Injectable({
  providedIn: 'root'
})
export class ActPeriodWorkingService {

  private url = environment.apiUrl + environment.dataHandlerUrl + "/act-period-working";

  constructor(private http: HttpClient) {
  }

  getDataById(id: string) {
    const endpoint = this.url + "/" + id;

    return this.http.get<ActPeriodWorking>(endpoint);
  }

  saveData(body: ActPeriodWorking) {
    this.http.post(this.url, body, httpOptions).subscribe(data => console.info('data', data));
  }
}
