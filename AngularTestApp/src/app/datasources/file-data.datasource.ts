import {DataSource} from "@angular/cdk/table";
import {BehaviorSubject, Observable, of} from "rxjs";
import {FileData, FileDataResponse} from "../models/file-data";
import {FileDataService} from "../services/file-data.service";
import {CollectionViewer} from "@angular/cdk/collections";
import {catchError, finalize} from "rxjs/operators";

export class FileDataDatasource implements DataSource<FileData> {

  private fileDataSubject = new BehaviorSubject<FileData[]>([]);
  private loadingSubject = new BehaviorSubject<boolean>(false);
  private countSubject = new BehaviorSubject<number>(0);
  public counter$ = this.countSubject.asObservable();
  public loading$ = this.loadingSubject.asObservable();

  constructor(private fileDataService: FileDataService) {
  }

  connect(collectionViewer: CollectionViewer): Observable<FileData[]> {
    return this.fileDataSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this.fileDataSubject.complete();
    this.loadingSubject.complete();
    this.countSubject.complete();
  }

  getAll(filter = '', pageNumber = 0, pageSize = 9, sortDirection = 'asc') {
    this.loadingSubject.next(true);
    this.fileDataService.getAll({fileName: filter, page: pageNumber, size: pageSize, sort: sortDirection})
      .pipe(
        catchError(() => of([])),
        finalize(() => this.loadingSubject.next(false))
      )
      .subscribe((result: FileDataResponse) => {
          this.fileDataSubject.next(result.content);
          this.countSubject.next(result.totalElements);
        }
      );
  }

}
